# km77.com

## Pasos Base (arrancar la aplicación)

Ir al directorio base del proyecto usando una consola

```
cp Vagrantfile.dist Vagrantfile

vagrant up --provision
```

Una vez terminado (la primera vez tarda un buen rato) se puede abrir en un navegador este enlace: [10.0.0.200](http://10.0.0.200)


## Maquetación


```
vagrant ssh

cd /vagrant/

rm -rf app/cache/prod/*

app/console assetic:watch --env=prod
```

Cada vez que se modifica un fichero less o uno js el "watch" lo vee y regenera el css y el js que se usa en la página



### ¿Dónde están los ficheros?

| Tipo | Directorio |
| ------------- | ------------- |
| LESS | app/Resources/public/less/ |
| JS | app/Resources/public/js/ |
| TWIG base | app/Resources/views/base.html.twig |
| TWIGS páginas | src/Km77/Bundle/AppBundle/Resources/views/ |

Para ver una página usar la parte del nombre de su twig sin ".html.twig" ejemplo: PF-Precios.html.twig -> [http://10.0.0.200/PF-Precios](http://10.0.0.200/PF-Precios)


### ¿Cómo lo cierro?

```
Ctrl-C # Para parar el watch

exit # Para salir de la máquina virtual
```

Así se llega de vuelta a la consola de origen. Para apagar la máquina virtual:


```
vagrant halt
```
