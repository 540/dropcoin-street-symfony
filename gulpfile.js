// File: Gulpfile.js
'use strict';

var gulp    = require('gulp'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload,
    jshint  = require('gulp-jshint'),
    stylish = require('jshint-stylish');

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "dropcoin-street.local"
    });

    gulp.watch(['src/DropcoinStreet/AppBundle/Resources/views/*.html.twig', 'app/Resources/views/*.html.twig']).on("change", browserSync.reload);
});

gulp.task('jshint', function() {
    return gulp.src(['gulpfile.js', './assets/bundles/**/js/*.js'])
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('fail'));
});

gulp.task('font-awesome', function() {
    gulp.src(['assets/vendor/fontawesome/fonts/**/*.{ttf,woff,eof,svg}', 'assets/vendor/fontawesome/css/font-awesome.min.css'], { base: 'assets/vendor/fontawesome' })
        .pipe(gulp.dest('web/fonts/font-awesome'));
});

gulp.task('jquery', function() {
    return gulp.src('assets/vendor/jquery/jquery.min.js')
        .pipe(gulp.dest('web/js/vendor/jquery'));
});

gulp.task('bootstrap', function() {
    return gulp.src('assets/vendor/bootstrap/dist/css/bootstrap.min.css')
        .pipe(gulp.dest('web/css/vendor/bootstrap'));
});

gulp.task('bootstrap-js', function() {
    return gulp.src('assets/vendor/bootstrap/dist/js/bootstrap.min.js')
        .pipe(gulp.dest('web/js/vendor/bootstrap'));
});

gulp.task('css', function() {
    return gulp.src('assets/bundles/dropcoinstreetapp/css/main.css')
        .pipe(gulp.dest('web/css'))
        .pipe(browserSync.stream());
});

gulp.task('js', function() {
    return gulp.src('assets/bundles/dropcoinstreetapp/js/main.js')
        .pipe(gulp.dest('web/js'))
        .pipe(browserSync.stream());
});

gulp.task('watch', function() {
    gulp.watch(['assets/bundles/**/css/*.css'], ['css']);
    gulp.watch(['assets/bundles/**/js/*.js'], ['jshint']);
});

gulp.task('default', ['browser-sync', 'font-awesome', 'jquery', 'bootstrap', 'bootstrap-js', 'css', 'watch']);
