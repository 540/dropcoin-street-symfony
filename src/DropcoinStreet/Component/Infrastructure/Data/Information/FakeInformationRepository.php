<?php

namespace Km77\Component\Infrastructure\Data\Information;

use Km77\Component\Domain\Model\Information\Information;
use Km77\Component\Domain\Model\Information\InformationFilter;
use Km77\Component\Domain\Model\Information\InformationRepository;

class FakeInformationRepository implements InformationRepository
{
    /**
     * @param InformationFilter $informationFilter
     *
     * @return string
     */
    public function findOne($informationFilter)
    {
        $make = $informationFilter->getMake();
        $model = $informationFilter->getModel();
        $modelYear = $informationFilter->getModelYear();
        $bodyStyle = $informationFilter->getBodyStyle();

        return new Information(
            '',
            '',
            '',
            'Información de '.$make.' '.$model.' '.$modelYear.' '.$bodyStyle,
            'Javier Moltó',
            '@javiermolto'
        );

    }
}