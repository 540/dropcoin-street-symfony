<?php

namespace Km77\Component\Domain\UseCases\Information;

use Km77\Component\Domain\Model\Information\InformationFilter;
use Km77\Component\Domain\Model\Information\InformationRepository;

class InformationFindUseCase
{
    /**
     * @var InformationRepository
     */
    private $informationRepository;

    public function __construct(InformationRepository $informationRepository)
    {
        $this->informationRepository = $informationRepository;
    }

    /**
     * @param InformationFilter $informationFilter
     *
     * @return array
     */
    public function execute(InformationFilter $informationFilter)
    {
        return $this->informationRepository->findOne($informationFilter);
    }
}