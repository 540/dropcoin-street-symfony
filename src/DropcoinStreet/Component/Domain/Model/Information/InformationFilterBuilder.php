<?php

namespace Km77\Component\Domain\Model\Information;

class InformationFilterBuilder
{
    /**
     * @var string
     */
    private $make;

    /**
     * @var string
     */
    private $model;

    /**
     * @var string
     */
    private $modelYear;

    /**
     * @var string
     */
    private $bodyStyle;

    /**
     * @param string $make
     *
     * @return $this
     */
    public function withMake($make)
    {
        $this->make = $make;

        return $this;
    }

    /**
     * @param string $model
     *
     * @return $this
     */
    public function withModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @param string $modelYear
     *
     * @return $this
     */
    public function withModelYear($modelYear)
    {
        $this->modelYear = $modelYear;

        return $this;
    }

    /**
     * @param string $bodyStyle
     *
     * @return $this
     */
    public function withBodyStyle($bodyStyle)
    {
        $this->bodyStyle = $bodyStyle;

        return $this;
    }

    /**
     * @return InformationFilter
     */
    public function build()
    {
        return new InformationFilter($this->make, $this->model, $this->modelYear, $this->bodyStyle);
    }
}