<?php

namespace Km77\Component\Domain\Model\Information;

class InformationFilter 
{
    /**
     * @var string
     */
    private $make;

    /**
     * @var string
     */
    private $model;

    /**
     * @var string
     */
    private $modelYear;

    /**
     * @var string
     */
    private $bodyStyle;

    /**
     * @param string $make
     * @param string $model
     * @param string $modelYear
     * @param string $bodyStyle
     */
    public function __construct($make, $model, $modelYear, $bodyStyle)
    {
        $this->make = $make;
        $this->model = $model;
        $this->modelYear = $modelYear;
        $this->bodyStyle = $bodyStyle;
    }

    public static function builder()
    {
        return new InformationFilterBuilder();
    }

    /**
     * @return string
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return string
     */
    public function getModelYear()
    {
        return $this->modelYear;
    }

    /**
     * @return string
     */
    public function getBodyStyle()
    {
        return $this->bodyStyle;
    }
}