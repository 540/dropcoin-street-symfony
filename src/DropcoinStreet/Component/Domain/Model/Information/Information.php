<?php

namespace Km77\Component\Domain\Model\Information;

class Information
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $headline;

    /**
     * @var string
     */
    private $photo;

    /**
     * @var string
     */
    private $body;

    /**
     * @var string
     */
    private $author;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $updateAt;

    /**
     * @param string $title
     * @param string $headline
     * @param string $photo
     * @param string $body
     * @param string $author
     * @param string $user
     * @param string $updateAt
     */
    function __construct(
        $title = null,
        $headline = null,
        $photo = null,
        $body = null,
        $author = null,
        $user = null,
        $updateAt = null
    ) {
        $this->title = $title;
        $this->headline = $headline;
        $this->photo = $photo;
        $this->body = $body;
        $this->author = $author;
        $this->user = $user;
        $this->updateAt = $updateAt;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getHeadline()
    {
        return $this->headline;
    }

    /**
     * @param string $headline
     */
    public function setHeadline($headline)
    {
        $this->headline = $headline;
    }

    /**
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param string $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * @param string $updateAt
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;
    }
}