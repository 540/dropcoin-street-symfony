<?php

namespace Km77\Component\Domain\Model\Information;

interface InformationRepository
{
    /**
     * @param InformationFilter $informationFilter
     *
     * @return string
     */
    public function findOne($informationFilter);
}
