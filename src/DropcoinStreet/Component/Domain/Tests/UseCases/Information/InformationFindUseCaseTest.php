<?php

namespace Km77\Component\Domain\Tests\UseCases\Creator;

use Km77\Component\Domain\Model\Information\InformationFilter;
use Km77\Component\Domain\Tests\BaseTestCase\ProphecyBaseTestCase;
use Km77\Component\Domain\UseCases\Information\InformationFindUseCase;

class InformationFindUseCaseTest extends ProphecyBaseTestCase
{
    protected function setUp()
    {
        parent::setUp();
    }

    public function testThatGetsMakeInformation()
    {
        $informationRepository = $this->prophesize('Km77\Component\Domain\Model\Information\InformationRepository');
        $informationGetUseCase = new InformationFindUseCase($informationRepository->reveal());
        $informationFilterBuilder = InformationFilter::builder();
        $informationFilter = $informationFilterBuilder->withMake('::make::')->build();

        $informationGetUseCase->execute($informationFilter);

        $informationRepository->findOne($informationFilter)->shouldBeCalled();
    }

    public function testThatGetsModelInformation()
    {
        $informationRepository = $this->prophesize('Km77\Component\Domain\Model\Information\InformationRepository');
        $informationGetUseCase = new InformationFindUseCase($informationRepository->reveal());
        $informationFilterBuilder = InformationFilter::builder();
        $informationFilter = $informationFilterBuilder->withModel('::model::')->build();

        $informationGetUseCase->execute($informationFilter);

        $informationRepository->findOne($informationFilter)->shouldBeCalled();
    }

    public function testThatGetsModelYearInformation()
    {
        $informationRepository = $this->prophesize('Km77\Component\Domain\Model\Information\InformationRepository');
        $informationGetUseCase = new InformationFindUseCase($informationRepository->reveal());
        $informationFilterBuilder = InformationFilter::builder();
        $informationFilter = $informationFilterBuilder->withModelYear('::modelYear::')->build();

        $informationGetUseCase->execute($informationFilter);

        $informationRepository->findOne($informationFilter)->shouldBeCalled();
    }
}
