<?php

namespace Km77\Component\Domain\Tests\BaseTestCase;

use PHPUnit_Framework_TestCase;
use Prophecy\Prophet;

class ProphecyBaseTestCase extends PHPUnit_Framework_TestCase
{
    /**
     * @var Prophet
     */
    private $prophet;

    protected function setUp()
    {
        $this->prophet = new Prophet();
    }

    protected function tearDown()
    {
        $this->prophet->checkPredictions();
    }

    protected function prophesize($class)
    {
        return $this->prophet->prophesize($class);
    }
}
