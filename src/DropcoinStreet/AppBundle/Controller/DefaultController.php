<?php

namespace DropcoinStreet\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function creatorAction($username)
    {
        try {
            return $this->render('DropcoinStreetAppBundle::'.$username.'.html.twig',['page' => $username]);
        } catch (\Exception $ex) {
            throw $this->createNotFoundException('The page does not exist');
        }
    }

    public function qrAction($username)
    {
        try {
            return $this->render('DropcoinStreetAppBundle::qr_'.$username.'.html.twig',['page' => $username]);
        } catch (\Exception $ex) {
            throw $this->createNotFoundException('The page does not exist');
        }
    }
}
